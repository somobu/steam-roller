# Steam roller

![](https://gitlab.com/somobu/steam-roller/-/raw/master/docs/screenshot.png)

It's like standard Steam discovery queue, but:
- next game is absolutely random, chosen from all known steam games;
- you have history and statistics;
- you have favorites list;


