import {get_favs} from "../../data";
import {GameSmallEntry} from "../GameSmallEntry";
import {Code, CodeWrapped, Header} from "./style";
import {Tab, TabList, TabPanel, Tabs} from "react-tabs";

import './react-tabs.css';

function s2csv(str) {
  return "\"" + str
  .replace("\n", " ")
  .replace("\"", "\"\"") + "\""
}

export function Favs() {

  return <>
    <Header>Favorites</Header>
    <Tabs>
      <TabList>
        <Tab>List</Tab>
        <Tab>JSON</Tab>
        <Tab>CSV</Tab>
        <Tab>Markdown (detailed)</Tab>
        <Tab>Markdown (shortened)</Tab>
      </TabList>

      <TabPanel>
        {
          get_favs().map(({id, title, icon, descr}) =>
            <GameSmallEntry id={id} title={title} icon={icon} descr={descr} key={id} deletable={true}/>
          )
        }
      </TabPanel>
      <TabPanel>
        <Code>
          {JSON.stringify(get_favs(), null, " ")}
        </Code>
      </TabPanel>
      <TabPanel>
        Columns: id, title, icon url, description
        <Code>
          {
            get_favs()
            .map(({id, title, icon, descr}) => id + "," + s2csv(title) + "," + s2csv(icon) + "," + s2csv(descr))
            .join("\n")
            + "\n\n"
          }
        </Code>
      </TabPanel>
      <TabPanel>
        <CodeWrapped>
          {
            get_favs()
            .map(({id, title, icon, descr}) => {
              return "# [" + title+"](https://s.team/a/"+id+")"
                + "\n\n![](" + icon + ")"
                + "\n\n" + descr
            })
            .join("\n\n\n")
          }
        </CodeWrapped>
      </TabPanel>
      <TabPanel>
        <CodeWrapped>
          {
            get_favs()
            .map(({id, title, icon, descr}) => "- [" + title+"](https://s.team/a/"+id+")")
            .join("\n")
          }
        </CodeWrapped>
      </TabPanel>
    </Tabs>
  </>
}