import styled from "styled-components";

export const Header = styled.h2`
  border-bottom: 1px solid gray;
`

export const Code = styled.pre`
  max-width: calc(100% - 2 * 8px);
  padding: 8px;
  overflow: scroll;
  background: #0c1b2d;
`

export const CodeWrapped = styled(Code)`
  white-space: pre-wrap;
`
