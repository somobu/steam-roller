import {BgImg} from "./style";

export function Background({url}) {
  return <BgImg src={url}/>
}