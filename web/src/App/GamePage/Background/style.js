import styled from "styled-components";

export const BgImg = styled.img`
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;

  width: 100%;
  height: 1080px;
  
  object-fit: cover;
  
  z-index: 0;
`