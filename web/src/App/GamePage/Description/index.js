import {Block, DescrRoot, H} from "./style";

export function Description({descr, about}) {

  return <DescrRoot>
    <H>Description</H>
    <Block dangerouslySetInnerHTML={{__html: descr}}/>
  </DescrRoot>

}