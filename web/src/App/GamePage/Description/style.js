import styled from "styled-components";

export const DescrRoot = styled.div`
  width: calc(800px);

  padding: 24px;
  margin-top: 48px;
  
  background: rgba(0, 0, 0, 0.25);
  border-radius: 8px;
  z-index: 1;
`;

export const H = styled.h2`
  margin: 0 0 18px;
  border-bottom: 1px solid gray;
`

export const Block = styled.div`
  font-size: 14px;
  line-height: 24px;

  & a {
    color: #47cbff;
  }

  & h2 {
    color: #016e9a;
    font-size: 18px;
    margin-bottom: 0;
  }
  
  & br {   
    display: block;
    margin: 0;
    padding: 0;
    height: 0;
  }
  
  & img {
    display: block;
    margin-left: auto;
    margin-right: auto;
  }
`