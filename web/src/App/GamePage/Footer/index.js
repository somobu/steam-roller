import {FooterBlock, FooterBtn, FooterSpacer} from "./style";
import {Stats} from "../../Stats";
import Modal from 'react-modal';
import {useState} from "react";
import {Favs} from "../../Favs";


const customStyles = {
  content: {
    top: '50%',
    left: '50%',
    right: 'auto',
    bottom: 'auto',
    marginRight: '-50%',
    transform: 'translate(-50%, -50%)',
    border: "none",
    maxHeight: '720px',

    width: '720px',
    padding: "0 16px 0 16px",

    background: "#1a293c",
    color: '#afafaf',
    font: "1.0em \"Fira Sans\", sans-serif"
  },
  overlay: {
    zIndex: 3,
    background: "rgba(0,0,0,0.72)"
  }
};

export function Footer({steamClick, favClick, nextClick, favListed}) {
  // Modals
  const [stats, setStats] = useState(false);
  const [favs, setFavs] = useState(false);

  //
  const [isFavListed, setFavListed] = useState(favListed)

  return <FooterBlock>
    <FooterBtn onClick={() => setStats(true)}>Stats</FooterBtn>
    <FooterBtn onClick={() => setFavs(true)}>Fav list</FooterBtn>
    <FooterSpacer/>
    <FooterBtn onClick={steamClick}>Steam</FooterBtn>

    <FooterBtn onClick={() => {
      setFavListed(!isFavListed);
      favClick()
    }}>
      {isFavListed ? "Unfav" : "Fav"}
    </FooterBtn>

    <FooterBtn onClick={nextClick}>Next</FooterBtn>

    <Modal onRequestClose={() => setStats(false)}
           style={customStyles}
           isOpen={stats}>
      <Stats/>
    </Modal>

    {favs ?
      <Modal onRequestClose={() => setFavs(false)}
             style={customStyles}
             isOpen={true}>
        <Favs/>
      </Modal>
      : <></>
    }

  </FooterBlock>
}