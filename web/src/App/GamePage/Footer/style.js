import styled from "styled-components";

export const FooterBlock = styled.div`
  width: 940px;
  height: 48px;

  padding-left: 16px;
  padding-right: 16px;
  padding-bottom: 16px;
  
  display: flex;
  flex-direction: row;
  align-content: flex-end;
  gap: 8px;

  background: rgba(0, 0, 0, 0.25);
  border-radius: 0 0 8px 8px;

  z-index: 1;
`

export const FooterSpacer = styled.div`
  flex-grow: 1;
`

export const FooterBtn = styled.div`
  width: 98px;
  
  margin-top: 4px;
  margin-bottom: 4px;

  border-radius: 6px;

  display: flex;
  justify-content: center;
  align-items: center;

  cursor: pointer;
  user-select: none;
  
  color: #cecece;
  background: #00436d;
  font-weight: 600;

  &:hover {
    background: #025e97;
  }
`