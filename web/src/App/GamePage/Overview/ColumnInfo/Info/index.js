import {InfoRoot, InfoRow, InfoSpacer, InfoText, InfoType, InfoValue} from "./style";

export function Info({text, releaseDate, dev, pub, genres, price}) {

  return <InfoRoot>
    <InfoText dangerouslySetInnerHTML={{__html: text}}/>
    <InfoSpacer/>
    <InfoRow>
      <InfoType>Release date</InfoType>
      <InfoValue>{releaseDate}</InfoValue>
    </InfoRow>
    <InfoSpacer/>
    <InfoRow>
      <InfoType>Developer</InfoType>
      <InfoValue>{dev}</InfoValue>
    </InfoRow>
    <InfoRow>
      <InfoType>Publisher</InfoType>
      <InfoValue>{pub}</InfoValue>
    </InfoRow>
    <InfoSpacer/>
    <InfoRow>
      <InfoType>Genres</InfoType>
      <InfoValue>{genres}</InfoValue>
    </InfoRow>
    {
      price == null ? <></> :
        <InfoRow>
          <InfoType>Price</InfoType>
          <InfoValue>{price}</InfoValue>
        </InfoRow>
    }
  </InfoRoot>
}