import styled from "styled-components";

export const InfoRoot = styled.div`
  padding: 8px 0 4px;
`

export const InfoSpacer = styled.div`
  height: 16px;
`

export const InfoText = styled.div`
  display: block;
  overflow: hidden;
  word-wrap: break-word;
  text-overflow: ellipsis;
  max-height: 106px;
  font-size: 14px;
  line-height: 18px;
`

export const InfoRow = styled.div`
  display: flex;
  flex-direction: row;
  font-size: 12px;
`

export const InfoType = styled.div`
  width: 108px;
  border: 1px black;
  font-weight: 700;
  
  flex-shrink: 0;
  flex-grow: 0;
`

export const InfoValue = styled.div`
`
