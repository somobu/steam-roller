import {ColumnRightRoot, LogoRoot} from "./style";
import {Info} from "./Info";

function Logo({url}) {
  return <LogoRoot>
    <img src={url}/>
  </LogoRoot>
}

export function ColumnInfo({data}) {
  return <ColumnRightRoot>
    <Logo url={data.header_image}/>
    <Info
      text={data.short_description}
      releaseDate={data.release_date.date}
      dev={data.developers.join(", ")}
      pub={data.publishers.join(", ")}
      genres={data.genres.map((value) => value.description).join(", ")}
      price={data.price_overview?.final_formatted || null}
    />

  </ColumnRightRoot>

}