import styled from "styled-components";

export const ColumnRightRoot = styled.div`
  width: 324px;
  height: 100%;
  flex-grow: 1;

  margin-left: 16px;
`

export const LogoRoot = styled.div`
  width: 324px;
  height: 151px;

  img {
    width: 100%;
    height: 100%;
  }
`