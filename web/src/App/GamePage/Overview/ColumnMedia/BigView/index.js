import {
  BiggerView,
  ContentImage,
  ContentView,
  OverlayBtnLeft,
  OverlayBtnRight,
  OverlayRoot,
  OverlaySpacer
} from "./style";


function ContentVid({url, bigger}) {
  return <video
    controls={true}
    width={bigger ? 920 : 600}
    height={bigger ? 516 : 337}
    autoPlay={true}
    muted={true}
  >
    <source src={url} type="video/mp4"/>
  </video>
}

function Overlay({onPrev, onNext}) {
  return <OverlayRoot>
    <OverlayBtnLeft onClick={onPrev}>{"<"}</OverlayBtnLeft>
    <OverlaySpacer/>
    <OverlayBtnRight onClick={onNext}>{">"}</OverlayBtnRight>
  </OverlayRoot>
}

export function BigView({screenshotUrl, videoUrl, onPrev, onNext, onClick, bigger}) {

  let contentView = null;
  if (screenshotUrl != null) {
    contentView = <ContentImage src={screenshotUrl} onClick={onClick}/>
  } else if (videoUrl != null) {
    contentView = <ContentVid url={videoUrl} bigger={bigger}/>
  }

  let content = <>
    {contentView}
    <Overlay
      onPrev={onPrev}
      onNext={onNext}
    />
  </>

  if (bigger) {
    return <BiggerView>{content}</BiggerView>
  } else {
    return <ContentView>{content}</ContentView>
  }

}