import styled from "styled-components";

export const ContentView = styled.div`
  position: relative;

  width: 600px;
  height: 337px;
  
  &:hover > div:last-child {
    display: flex;
  }
`

export const BiggerView = styled(ContentView)`
  width: 920px;
  height: 516px;
  overflow-y: clip;
`

export const ContentImage = styled.img`
  width: 100%;
  height: 100%;
  object-fit: contain;
  cursor: pointer;
  
`

export const OverlayRoot = styled.div`
  position: absolute;
  top: calc(50% - 24px);
  left: 0;
  width: 100%;
  height: 0;

  display: none;
  justify-content: center;
  align-items: center;

  user-select: none;
`

export const OverlayBtn = styled.div`
  width: 32px;
  height: 64px;

  display: flex;
  justify-content: center;
  align-items: center;

  cursor: pointer;
  
  background: rgba(0, 0, 0, 0.64);
`

export const OverlayBtnLeft = styled(OverlayBtn)`
  border-top-right-radius: 8px;
  border-bottom-right-radius: 8px;
`

export const OverlayBtnRight = styled(OverlayBtn)`
  border-top-left-radius: 8px;
  border-bottom-left-radius: 8px;
`

export const OverlaySpacer = styled.div`
  flex-grow: 1;
  pointer-events: none;
`