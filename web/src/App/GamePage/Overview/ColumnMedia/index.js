import {Carousel, ColumnLeftRoot, SwitchItemImg, SwitchItemRoot, SwitchItemRootActive, VidOverlayRoot} from "./style";
import {useEffect, useRef, useState} from "react";
import {BigView} from "./BigView";

import Arrow from './ic_play_arrow.png'
import Modal from "react-modal";


const customStyles = {
  content: {
    top: '50%',
    left: '50%',
    right: 'auto',
    bottom: 'auto',
    marginRight: '-50%',
    transform: 'translate(-50%, -50%)',
    border: "none",
    maxHeight: '720px',

    width: '920px',
    padding: "0",
    margin: "0",

    background: "#1a293c",
    color: '#afafaf',
    font: "1.0em \"Fira Sans\", sans-serif"
  },
  overlay: {
    zIndex: 3,
    background: "rgba(0,0,0,0.72)"
  }
};


function VidOverlay() {
  return <VidOverlayRoot><img src={Arrow}/></VidOverlayRoot>
}

function SwitchItem({url, onClick, isVid, active}) {

  const datView = useRef();

  useEffect(() => {
    if (active && datView.current != null) {
      datView.current.scrollIntoView(false);
    }
  }, [active])

  if (active) {
    return <SwitchItemRootActive ref={datView} onClick={onClick}>
      <SwitchItemImg src={url}/>
      {isVid ? <VidOverlay/> : <></>}
    </SwitchItemRootActive>
  } else {
    return <SwitchItemRoot ref={datView} onClick={onClick}>
      <SwitchItemImg src={url}/>
      {isVid ? <VidOverlay/> : <></>}
    </SwitchItemRoot>
  }
}

export function ColumnMedia({data}) {

  const [modalPreview, setModalPreview] = useState(false);

  const movie_ids = data.movies.map((value) => value.id)
  const scrnh_ids = data.screenshots.map((value) => value.id)
  const all_ids = movie_ids.concat(scrnh_ids)

  const [currentId, setId] = useState(all_ids[0])

  let screenshotUrl = data.screenshots.find(value => value.id === currentId)
  if (screenshotUrl != null) screenshotUrl = screenshotUrl.path_full

  let datVidUrl = data.movies.find(value => value.id === currentId)
  if (datVidUrl != null) datVidUrl = datVidUrl.mp4["480"]

  const ofsId = (offset) => {

    let currentIdx;
    if (screenshotUrl == null) currentIdx = data.movies.indexOf(data.movies.find(value => value.id === currentId))
    else currentIdx = data.movies.length + data.screenshots.indexOf(data.screenshots.find(value => value.id === currentId))

    currentIdx += offset;

    if (currentIdx >= all_ids.length) {
      currentIdx = 0
    } else if (currentIdx < 0) {
      currentIdx = all_ids.length - 1
    }

    setId(all_ids[currentIdx])
  }


  return <ColumnLeftRoot>
    <BigView
      screenshotUrl={modalPreview ? null : screenshotUrl}
      videoUrl={modalPreview ? null : datVidUrl}
      onPrev={() => ofsId(-1)}
      onNext={() => ofsId(1)}
      onClick={() => setModalPreview(true)}
      bigger={false}
    />

    <Carousel>
      {data.movies.map((value) =>
        <SwitchItem key={value.id}
                    url={value.thumbnail}
                    onClick={() => setId(value.id)}
                    isVid={true}
                    active={value.id === currentId}
        />)}
      {data.screenshots.map((value) =>
        <SwitchItem key={value.id}
                    url={value.path_thumbnail}
                    onClick={() => setId(value.id)}
                    isVid={false}
                    active={value.id === currentId}
        />)}
    </Carousel>

    {
      modalPreview ?
        <Modal onRequestClose={() => setModalPreview(false)}
               style={customStyles}
               isOpen={true}>
          <BigView
            screenshotUrl={screenshotUrl}
            videoUrl={datVidUrl}
            onPrev={() => ofsId(-1)}
            onNext={() => ofsId(1)}
            onClick={() => setModalPreview(false)}
            bigger={true}
          />
        </Modal>
        : <></>
    }

  </ColumnLeftRoot>
}