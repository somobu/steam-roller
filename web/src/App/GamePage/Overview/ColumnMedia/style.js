import styled from "styled-components";

export const ColumnLeftRoot = styled.div`
  height: 100%;
`


export const Carousel = styled.div`
  width: 600px;
  height: 76px;
  
  display: flex;
  flex-direction: row;
  gap: 4px;

  overflow-x: scroll;
  overflow-y: hidden;
`

export const SwitchItemRoot = styled.div`
  position: relative;
  width: 130px;
  height: 65px;
  
  margin-top: 4px;
  margin-bottom: 4px;
  
  flex-grow: 0;
  flex-shrink: 0;

  cursor: pointer;
`

export const SwitchItemRootActive = styled(SwitchItemRoot)`
  box-sizing: border-box;
  border: 2px solid white;
`

export const VidOverlayRoot = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;

  display: flex;
  align-items: center;
  justify-content: center;

  color: white;
  background: rgba(0, 0, 0, 0.6);

  & > img {
    width: 24px;
    height: 24px;
  }
`

export const SwitchItemImg = styled.img`
  width: 100%;
  height: 100%;
  object-fit: cover;
`