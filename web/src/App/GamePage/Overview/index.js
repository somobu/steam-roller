import {CentralBlock} from "./style";
import {ColumnMedia} from "./ColumnMedia";
import {ColumnInfo} from "./ColumnInfo";


export function Overview({data}) {
  return <CentralBlock>
    <ColumnMedia data={data}/>
    <ColumnInfo data={data}/>
  </CentralBlock>
}