import styled from "styled-components";

export const CentralBlock = styled.div`
  width: 940px;
  height: 414px;
  
  padding-left: 16px;
  padding-right: 16px;
  
  display: flex;
  flex-direction: row;

  background: rgba(0, 0, 0, 0.25);
  z-index: 1;
`
