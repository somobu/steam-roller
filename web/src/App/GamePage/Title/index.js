import {TitleBlock, TitleText} from "./style";

export function Title({name}) {
  return <TitleBlock>
    <TitleText>{name}</TitleText>
  </TitleBlock>
}
