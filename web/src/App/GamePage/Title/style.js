import styled from "styled-components";

export const TitleBlock = styled.div`
  width: 940px;
  height: 40px;

  padding: 14px 16px 8px;

  display: flex;
  align-items: center;

  background: rgba(0, 0, 0, 0.25);
  border-radius: 8px 8px 0 0;

  z-index: 1;
`

export const TitleText = styled.span`
  color: white;
  font-size: 22px;
  font-weight: 800;
`