import {CentralRoot, DatBtn} from "./style";
import {Title} from "./Title";
import {Footer} from "./Footer";
import {useEffect, useState} from "react";
import {Background} from "./Background";
import {add_to_favs, add_to_last, add_to_seen, is_fav, next_random_game, remove_from_favs} from "../../data";
import {Overview} from "./Overview";
import {Description} from "./Description";
import {useNavigate, useParams} from "react-router-dom";


export function GamePage() {

  const {id} = useParams();
  const navigate = useNavigate();

  const [loading, setLoading] = useState(false)
  const [loadedId, setLoadedId] = useState("-1")
  const [error, setError] = useState(null)
  const [data, setData] = useState(null);

  useEffect(
    () => {
      async function load() {

        try {
          const response = await fetch('https://store.steampowered.com/api/appdetails?appids=' + id, {
            method: 'GET',
            headers: {
              Accept: 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8',
            },
          });

          if (!response.ok) {
            throw new Error(`Error! status: ${response.status}`);
          }

          const result = await response.json();
          const data = result[id]
          if (!data.success) {
            if (!("data" in data)) data.data = {}
            data.data.steam_appid = id
            throw new Error(JSON.stringify(data));
          }

          if (!("movies" in data.data)) data.data.movies = []

          document.title = data.data.name + " | Steam roller";

          data.favlisted = is_fav(id)

          add_to_last(data)
          setError(null)
          setData(data);
        } catch (err) {
          setError(err.message);
        } finally {
          setLoading(false);
        }
      }

      if (loadedId !== id) {
        setLoading(true);
        add_to_seen(id)
        setLoadedId(id)
        load()
      }
    }, [id, loadedId]
  )

  const nextGame = () => {
    navigate("/" + next_random_game());
  };

  const toggleFav = () => {
    if (is_fav(id)) {
      remove_from_favs(id)
    } else {
      add_to_favs(data)
    }
  }

  const gotoSteam = () => {
    window.open("https://store.steampowered.com/app/" + id, '_blank').focus();
  }

  if (loading || !(error || data)) {
    return <CentralRoot>
      Loading...
    </CentralRoot>
  } else if (error !== null) {
    return <CentralRoot>
      <h2>Sorry, we got an error</h2>
      <p>The error text is as follows:<br/>{error}</p>
      <p>Maybe this game is unavailable in your region?
        <br/>You can{" "}
        <a href={"https://store.steampowered.com/app/" + id} target="_blank" rel="noreferrer">
          open game's Steam page to check.
        </a>
      </p>
      <DatBtn onClick={nextGame}>Next</DatBtn>
    </CentralRoot>
  } else {
    return <CentralRoot>
      <Background url={data.data.background}/>

      <Title name={data.data.name}/>
      <Overview data={data.data}/>

      <Footer
        steamClick={gotoSteam}
        favClick={toggleFav}
        nextClick={nextGame}
        favListed={data.favlisted}
      />

      <Description
        about={data.data.about_the_game}
        descr={data.data.detailed_description}
      />

    </CentralRoot>
  }

}