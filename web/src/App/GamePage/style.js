import styled from "styled-components";

export const CentralRoot = styled.div`
  width: 100%;

  padding-top: 100px;
  padding-bottom: 100px;
  
  display: flex;
  flex-direction: column;
  align-items: center;

  color: #afafaf;
  background: #1a293c;
  font: 1.0em "Fira Sans", sans-serif;
  
  & a {
    color: #47cbff;
  }
  
  & p {
    width: 400px;
  }
`

export const DatBtn = styled.div`
  width: 98px;
  height: 48px;
  margin: 4px;

  border-radius: 6px;

  display: flex;
  justify-content: center;
  align-items: center;

  cursor: pointer;
  user-select: none;
  
  color: #cecece;
  background: #00436d;
  font-weight: 600;

  &:hover {
    background: #025e97;
  }
`