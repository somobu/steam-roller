import {DelImg, Descr, FavsRoot, Img, Title} from "./style";
import {useState} from "react";

import IcDelete from "./ic_delete_outline.png"
import {remove_from_favs} from "../../data";

export function GameSmallEntry({id, title, icon, descr, deletable}) {

  console.log("Rerender!")

  const [deleted, setDeleted] = useState(false)

  const onDelete = (ev) => {
    ev.preventDefault()
    ev.stopPropagation()
    setDeleted(true)
    remove_from_favs(id)
  }

  if (deleted) return <></>

  return <FavsRoot to={"/" + id}>
    <Img src={icon}/>
    <div>
      <Title>{title}</Title>
      <Descr>{descr}</Descr>
    </div>

    {deletable ? <DelImg src={IcDelete} onClick={onDelete}/> : <></>}

  </FavsRoot>
}
