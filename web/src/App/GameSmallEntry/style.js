import styled from "styled-components";
import {Link} from "react-router-dom";

export const FavsRoot = styled(Link)`
  height: 107px;

  display: flex;
  flex-direction: row;

  margin-bottom: 16px;

  cursor: pointer;
  color: #afafaf;
  text-decoration: none;

  &:hover {
    border-radius: 8px;
    background: rgba(0, 0, 0, 0.42);
  }

  &:hover img {
    border-top-left-radius: 8px;
    border-bottom-left-radius: 8px;
  }
`

export const Img = styled.img`
  width: 230px;
  height: 107px;
  object-fit: cover;

  margin-right: 16px;

  flex-grow: 0;
  flex-shrink: 0;
`

export const Title = styled.h2`
  font-weight: 600;
  font-size: 12px;
  padding: 4px 0 0;
  margin: 0;
`

export const Descr = styled.span`
  font-size: 10px;
`

export const DelImg = styled.img`
  width: 12px;
  height: 12px;
  padding: 6px;

  border-radius: 6px;
  pointer-events: all;

  &:hover {
    background: #023c69;
  }
`
