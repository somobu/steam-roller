import {get_last, get_seen} from "../../data";
import {get_all_sids} from "../../sids";
import {GameSmallEntry} from "../GameSmallEntry";
import {Header} from "./style";


export function Stats() {

  const seen = get_seen().length
  const total = get_all_sids().length

  const percent = (seen / total) * 100

  return <>
    <Header>Statistics</Header>
    Total seen: {percent.toFixed(2)}% ({seen} of {total} games in Steam).

    <Header>Last 50 games</Header>
    {
      get_last().map(({id, title, icon, descr}) =>
        <GameSmallEntry id={id} title={title} icon={icon} descr={descr} key={id}/>
      )
    }
  </>;

}