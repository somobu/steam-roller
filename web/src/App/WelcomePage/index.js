import {FooterBtn, WelcomeContainer, WelcomeRoot} from "./style";
import {next_random_game} from "../../data";
import {useNavigate} from "react-router-dom";

export function WelcomePage() {

  const navigate = useNavigate();

  const nextGame = () => {
    navigate("/" + next_random_game());
  };

  return <WelcomeRoot>
    <WelcomeContainer>
      <h2>Hello!</h2>
      You can roll next game right here:
      <FooterBtn onClick={nextGame}>Next game</FooterBtn>
    </WelcomeContainer>
  </WelcomeRoot>
}