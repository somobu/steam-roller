import styled from "styled-components";

export const WelcomeRoot = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;

  display: flex;
  align-items: center;
  justify-content: center;
`

export const WelcomeContainer = styled.div`
  width: 480px;

  padding: 14px 16px 24px;
  background: rgba(0, 0, 0, 0.25);
  border-radius: 8px;

  color: #afafaf;
  font: 1.0em \\"Fira Sans\\", sans-serif;
`

export const FooterBtn = styled.div`
  height: 48px;
  width: 108px;
  
  margin-top: 18px;
  margin-bottom: 4px;

  border-radius: 6px;

  display: flex;
  justify-content: center;
  align-items: center;

  cursor: pointer;
  user-select: none;
  
  color: #cecece;
  background: #00436d;
  font-weight: 600;

  &:hover {
    background: #025e97;
  }
`