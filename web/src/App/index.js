import React from "react";
import {GamePage} from "./GamePage";
import {BrowserRouter, Route, Routes} from "react-router-dom";
import {WelcomePage} from "./WelcomePage";

export function App() {

  return <>
    <BrowserRouter>
      <Routes>
        <Route path="" element={<WelcomePage/>}/>
        <Route path="/:id" element={<GamePage/>}/>
      </Routes>
    </BrowserRouter>
  </>
}