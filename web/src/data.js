import {get_all_sids} from "./sids";

export function get_seen() {
  const seen = localStorage.getItem("seen");

  let data = []
  if (seen != null) data = JSON.parse(seen)

  return data
}

export function add_to_seen(game_id) {
  let data = get_seen()
  let game = Number(game_id)
  data.push(game)

  localStorage.setItem("seen", JSON.stringify(data))
}

export function next_random_game() {
  const known = get_all_sids()
  const seen = get_seen()

  const unseen = known.filter(val => !seen.includes(val))

  return unseen[Math.floor(Math.random() * unseen.length)];
}


export function get_favs() {
  const favs = localStorage.getItem("favs");

  let data = []
  if (favs != null) data = JSON.parse(favs)

  return data
}

export function add_to_favs(data) {
  let favs = get_favs()

  let found = favs.find(value => value.id === data.data.steam_appid)
  if (found) return

  const favData = {
    id: data.data.steam_appid,
    title: data.data.name,
    icon: data.data.header_image,
    descr: data.data.short_description
  }

  favs = [favData].concat(favs)
  localStorage.setItem("favs", JSON.stringify(favs))
}

export function remove_from_favs(id) {
  let favs = get_favs()
  favs = favs.filter(val => val.id !== Number(id))
  localStorage.setItem("favs", JSON.stringify(favs))
}

export function is_fav(id) {
  let favs = get_favs()
  let found = favs.find(value => value.id === Number(id))
  return !!found;
}


export function get_last() {
  const last = localStorage.getItem("last");

  let data = []
  if (last != null) data = JSON.parse(last)

  return data
}

export function add_to_last(data) {
  let favs = get_last()
  favs = favs.filter(value => value.id !== data.data.steam_appid)

  const favData = {
    id: data.data.steam_appid,
    title: data.data.name,
    icon: data.data.header_image,
    descr: data.data.short_description
  }

  favs = [favData].concat(favs).slice(0, 50)

  localStorage.setItem("last", JSON.stringify(favs))
}